variable "namerg" {
  type        = string
  description = "Nome do Resource Group"
  default     = "rg-variaveis"
}

variable "location" {
  type        = string
  description = "Localização dos recursos do Azure. Ex: brazilsouth"
  #default     = "eastus"
}

variable "tags" {
  type        = map(any)
  description = "Tags nos Recursos e Serviços do Azure."
  default = {
    ambiente    = "desenvolvimento"
    responsavel = "André Ribeiro"
  }
}

variable "vnetenderecos" {
  type    = list(any)
  default = ["10.0.0.0/24", "192.168.0.0/24"]
}