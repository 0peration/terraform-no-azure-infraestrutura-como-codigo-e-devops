provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "rg_terraform_import"
  location = "brazilsouth"
  tags = {
    "ambiente" = "treinamento"
  }
}

resource "azurerm_virtual_network" "vnet" {
  name                = "vnet_terraform_import"
  resource_group_name = "rg_terraform_import"
  location            = "brazilsouth"
  address_space       = ["10.0.0.0/16", "192.168.0.0/16"]
  tags = {
    "ambiente" = "teste"
  }
}

resource "azurerm_network_security_group" "nsg" {
  
}