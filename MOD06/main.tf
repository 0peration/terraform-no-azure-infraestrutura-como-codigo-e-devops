terraform {

  backend "azurerm" {
    resource_group_name  = "stateterraformcloud"
    storage_account_name = "stateterraformcloud"
    container_name       = "terraformstate"
    key                  = "DaT/OsWFdnd1Yo9XFFhT7Oc7c6LdnegGqcahDTxVp8OuosGQneC9YmiML6fEnL3R2qMWswZ+6UYM+AStucQsug=="
  }
}

provider "azurerm" {
  features {
  }
}

resource "azurerm_resource_group" "rg-state" {
  name     = "rg_terraform_com_state"
  location = "brazilsouth"
}