terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
    }
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "random" {
}

provider "azurerm" {
  features {
  }
}

resource "azurerm_resource_group" "rgstorage" {
  name     = "storagerg"
  location = "brazilsouth"
}

resource "random_string" "random" {
  length  = 3
  special = false
  upper   = false
  numeric = true
}

resource "azurerm_storage_account" "cloudstoragetf" {
  name                     = "tftreinamento${random_string.random.result}"
  resource_group_name      = "storagerg"
  location                 = "brazilsouth"
  account_tier             = "Standard"
  access_tier              = "Hot"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "container" {
  name                 = "terraform"
  storage_account_name = azurerm_storage_account.cloudstoragetf.name
  depends_on           = [azurerm_storage_account.cloudstoragetf]
}

output "blobstorage-name" {
  value = azurerm_storage_account.cloudstoragetf.name
}

output "blobstorage-chave-primaria" {
  value     = azurerm_storage_account.cloudstoragetf.primary_access_key
  sensitive = true
}

output "blobstorage-chave-secundaria" {
  value     = azurerm_storage_account.cloudstoragetf.secondary_access_key
  sensitive = true
}